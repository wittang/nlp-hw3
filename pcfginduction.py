#Wilson Tang
#107513801
#CSE390

import tree
import sys
import operator

unary_Rules = {}
binary_Rules = {}
alpha_Counts = {}

def dict_update(dict, key):
	if key in dict:
		dict[key] +=1
	else:
		dict[key] = 1

def merge_two_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z

def print_Top10():
	all_Rules = merge_two_dicts(unary_Rules, binary_Rules)
	sorted_rules = sorted(all_Rules.items(), key=operator.itemgetter(1))
	sorted_rules.reverse()
	count2 = 0
	while count2 < 10:
		print(sorted_rules[count2])
		count2 += 1

def traverse(t):
	children = t.children
	pos = 0
	while pos < len(children):
		subTree = (children[pos])
		if subTree in tree.Node.leaves(children[pos]):
			unary_Rule = subTree.parent.label + " -> " + subTree.label
			dict_update(unary_Rules, unary_Rule)
			dict_update(alpha_Counts, subTree.parent.label)
		elif pos == (len(children) - 1):
			binary_Rule = subTree.parent.label + " -> " + subTree.parent.children[pos-1].label + " " + subTree.label
			dict_update(binary_Rules, binary_Rule)
			dict_update(alpha_Counts, subTree.parent.label)
		traverse(subTree)
		pos += 1

def calc_MLE(rule):
	alpha = rule.split()[0]
	alpha_Count = alpha_Counts[alpha]
	rule_Count = binary_Rules[rule]
	return float(rule_Count)/alpha_Count

def calc_uni_L(rule):
	rule_Count = unary_Rules[rule]
	return float(rule_Count + 1)/(sum(unary_Rules.values()) + sum(binary_Rules.values()) + len(unary_Rules) + len(binary_Rules) + 1)

treeFile = open(sys.argv[1])
treeLines = treeFile.readlines();
count = 0
for line in treeLines:
	t = tree.Tree.from_str(line)
	traverse(t.root)
	count += 1
tempUnary_Rules = {}
for rule in unary_Rules:
	terminal = rule.split()[0]
	unkRule = terminal + " -> <unk>"
	if unkRule not in unary_Rules:
		tempUnary_Rules[unkRule] = 0
		dict_update(alpha_Counts, terminal)
unary_Rules = merge_two_dicts(unary_Rules, tempUnary_Rules)
output_File = open("PCFG.txt", "w")
for rule in unary_Rules:
	output_File.write(rule + "\t" + str(unary_Rules[rule]) + "\t" + str(calc_uni_L(rule)) + "\n")
for rule in binary_Rules:
	output_File.write(rule + "\t" + str(binary_Rules[rule]) + "\t" + str(calc_MLE(rule)) + "\n")
output_File.close()

print_Top10()









