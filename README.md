Wilson Tang
107513801
CSE390

Necessary Files:
cykparser.py
evalb.py
pcfginduction.py
test.trees
train.trees
test.txt
tree.py

To begin: Open terminal and navigate to location of files

pcfginduction.py
To train the model, in the terminal, enter: python pfginduction.py train.trees
where train.txt is the training set

cykparser.py
To run the CYK parser, in the terminal, enter: python cykparser.py test.txt 
test.txt is the test set the parser will create trees for
This will generate the file output.trees which will be used for evaluation purposes

evalb.py
To run CYK evaluation, in the terminal, enter: python evalb.py output.trees test.trees
where output.trees is the output after running cykparser.py 
and test.trees is what the output will be compared to

TOP 10 MOST FREQUENT RULES WITH FREQUENCIES:
PUNC -> .		383
TO -> to		268
PP -> IN NP_NNP	263
IN -> from 		243
PP -> IN NP 	222
PP -> TO NP_NNP	175
NNS -> flights 	169
NP -> NNP NNP 	151
PUNC -> ? 		144
DT -> the 		137