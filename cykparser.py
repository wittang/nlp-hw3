#Wilson Tang
#107513801
#CSE390

import tree
import sys
import operator

grammar = {}
treeString = ""
trees = []
probs = []
probabilities = []

def get_Grammar():
   rules = open("PCFG.txt", "r")
   ruleLines = rules.readlines()
   for line in ruleLines:
      ruleData = line.split('\t')
      rule = ruleData[0]
      prob = ruleData[2]
      grammar[rule] = float(prob)

def cleanUp(str):
   newString = ""
   words = str.split()
   if words[0][0] != "T":
      newString += "TOP("
   for index in range(len(words)):
      if words[index] == ")":
         newString += words[index]
      elif index == 0:
         newString += words[index]
      else:
         newString += " " + words[index]
   return newString
      
def create_Table(length):
   table = []
   for i in range(length):
      j = 0
      while j <= length:
         if j == 0:
            table.append([])
         else:
            table[i].append([])
         j += 1
   return table


def findBP(score, back, start, end, label):
   bps = []
   probs = []
   (A, (split, B, C)) = back[start][end][0]
   try:
      for index in range(len(back[start][end])):
         (a, prob) = score[start][end][index]
         (A, (split, B, C)) = back[start][end][index]
         if A == label:
            bps.append((A, (split, B, C)))
            probs.append(prob)
      return bps[probs.index(max(probs))]
   except:
      return (A, (split, B, C))


def buildTree(score, back, root):
   for i in range(len(back)):
      for j in range(len(back[i])):
         if root in back[i][j]:
            (A, (split, B, C)) = root
            parentNode = A + "("
            subtreeB = B + "("
            backPointer = findBP(score, back, i, split, B)
            if backPointer[1][0] != -1:
               subtreeB = buildTree(score, back, backPointer)
            parentNode += subtreeB + ") "
            backPointer = findBP(score, back, split, j, C)
            subtreeC = C + "("
            if backPointer[1][0] != -1:
               subtreeC = buildTree(score, back, backPointer)
            parentNode += subtreeC + ")"
            return parentNode


def CKY(words, grammar, outputFile):
   global trees
   global probabilities
   trees = []
   probabilities = []

   score = create_Table(len(words) + 1) 
   back = create_Table(len(words) + 1) 
   for i in range(len(words)):
      begin = i
      end = i + 1

      for rule in grammar:
         ruleInfo = rule.split()
         A = ruleInfo[0]
         B = ruleInfo[2]
         if B == words[begin]:
            score[begin][end].append((A, grammar[rule]))
         elif B == "<unk>":
            score[begin][end].append((A, grammar[rule]))

      back[begin][end].append((A, (-1, B, None)))   

   for span in range(2,len(words) +1): 
      for begin in range(len(words) - span + 1):
         end = begin + span 
         for split in range((begin), (end-1)):
            split +=1
            for rule in grammar:
               ruleInfo = rule.split()
               if len(ruleInfo) > 3:
                  A = ruleInfo[0]
                  B = ruleInfo[2]
                  C = ruleInfo[3]
                  maxProb = 0
                  maxScore = ()
                  maxbackPointer = ()
                  for (b, b_prob) in score[begin][split]:
                     for (c,c_prob) in score[split][end]:
                        if B == b  and C == c:
                           prob = b_prob * c_prob * grammar[rule]
                           for (a, a_prob) in score[begin][end]:
                              if A == a:
                                 if prob > a_prob and prob > maxProb:
                                    maxProb = prob
                                    maxScore = (A,prob)
                                    maxbackPointer = (A, (split, B, C))
                           else:
                              if prob > maxProb:
                                 maxProb = prob
                                 maxScore = (A,prob)
                                 maxbackPointer = (A, (split, B, C))
                  if maxProb > 0:
                     score[begin][end].append(maxScore)
                     back[begin][end].append(maxbackPointer)
   
   top = findBP(score, back, 0, len(words), "TOP")
   tree =  buildTree(score, back, top)
   tree += ")"
   officialTree = ""
   treesplit = tree.split("()")
   if len(treesplit) == len(words)+1:
      print("first")
      for index in range(len(words)):
         officialTree += treesplit[index] + "(" + words[index] + ")" 
   elif len(treesplit) > len(words)+1:
      print("2")
      for index in range(len(words)):
         officialTree += treesplit[index] + "(" + words[index] + ")" 
   else:
      print("3")
      for index in range(len(treesplit) - 1):
         officialTree += treesplit[index] + "(" + words[index] + ")" 
   outputFile.write(officialTree + ")\n")
   print(officialTree + ")")

get_Grammar()
test = open("test.txt", "r")
testLines = test.readlines()
outputFile = open("output.trees", "w")
for line in testLines:
   CKY(line.split(), grammar, outputFile)
